
from pygame.locals import *
import pygame

#import debug_memori
#import gc


class World:

    def __init__(self, scene):
        """
        Die Wurzel des kompletten Programms,
        die immer die aktuell zugewiesene Szene darstellt.
        """

        pygame.key.set_repeat(1, 1)
        pygame.mouse.set_visible(1)
        self.clock = pygame.time.Clock()
        self.set_scene(scene)

    def set_scene(self, scene):
        """Hier wird eine neue darzustellende Szene uebergeben."""

        scene.master = self
        try:
            del self.representation.master
        except AttributeError:
            print("keine scene zum loeschen vorhanden.")
        self.representation = scene
        self.key_dict = {MOUSEMOTION : scene.mouse_motion,
                         MOUSEBUTTONUP :  scene.mouse_up,
                         MOUSEBUTTONDOWN : scene.mouse_down,
                         KEYDOWN : scene.key_down,
                         VIDEORESIZE : scene.resize,
                         KEYUP : scene.key_up}
        #gc.collect(2)
        #print(gc.garbage)

    def input_handling(self):
        """Hier werden die Input Methoden der uebergebenen Szene aufgerufen."""

        for py_event in pygame.event.get():
            event = self.key_dict.get(py_event.type, False)
            self.representation.event(py_event)
            if event:
                event(py_event)

    def main(self):
        """Startet das Programm."""

        input_handling = self.input_handling
        clock = self.clock
        while self.representation:
            input_handling()
            self.representation.update(
                past_time=clock.tick(self.representation.fps))
            self.representation.update_display()
            pygame.display.flip()
