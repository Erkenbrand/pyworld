
from pygame.locals import *
import pygame


class Scene:
    """
    Die Szene die von der Welt behandelt wird,
    alle Szenen sollten von dieser klasse erben.
    """
    def __init__(self, name="pygame Fenster", fps=30, aufloesung=(600, 600),
                 flags=RESIZABLE):
        """Erstellt eine Scene die einer Welt übergeben werden muss."""

        self.aufloesung = aufloesung
        self.fps = fps
        self.flags = flags
        pygame.display.set_caption(name)
        self.image = pygame.display.set_mode(aufloesung, flags)
        self.screen_array = self.image.get_rect()
        self.master = None

    def init(self):
        self.master.set_scene(self)

    def update(self, past_time):
        """
        Hier koennen Updates durchgefuehrt werden.
        es steht der Parameter past_time zur Verfuegung.
        """
        pass

    def update_display(self):
        """Hier muss die Szene auf self.image gezeichnet werden."""
        pass

    def event(self, event):
        """
        Wird fuer jedes event das von pygame.event.get() zurueckgegebenen wierd,
        aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def key_down(self, event):
        """
        Wird beim druecken einer Taste aufgerufen. Der Parameter event,
        kann zur Behandlung des entsprechenden pygame events benutzt werden.
        """
        pass

    def key_up(self, event):
        """
        Wird beim loslassen einer Taste aufgerufen. Der Parameter event,
        kann zur Behandlung des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_motion(self, event):
        """
        Wird beim bewegen der Maus, ueber dem pygame Fenster aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_up(self, event):
        """
        Wird beim loslasen eines Maus buttons aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def mouse_down(self, event):
        """
        Wird beim druecken eines Maus buttons aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """
        pass

    def resize(self, event):
        """
        Wird beim verendern der groesse, des pygame.display aufgerufen.
        Der Parameter event, kann zur Behandlung
        des entsprechenden pygame events benutzt werden.
        """

        self.aufloesung = event.size
        self.image = pygame.display.set_mode(event.size, self.flags)
        self.screen_array = self.image.get_rect()
